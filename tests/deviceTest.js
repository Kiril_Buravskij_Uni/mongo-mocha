const assert = require('assert');
const UserChar = require('../models/userListModel')

describe('Duomenu bazes User modelio Device saraso testai', () => {
//
    var user = new UserChar({
        name: 'Edgaras',
        user_type: 'basic',
        phone: +3706857554,
        active: false,
        devices: [{
            alias: 'lexus_cam',
            device_type: 'camera',
            ip_address: '172.30.453.81',
            active: true
        },
        {
            alias: 'lexus_motion',
            device_type: 'motion',
            ip_address: '172.30.453.82',
            active: true
        }]
    });

    it('Pridejimo prietaiso testas', (done) => {
        user.save().then( (record) => {
            assert(record.name === 'Edgaras');
            assert(record.user_type !== 'premium');
            assert(record.phone !== +37063847554);
            assert(record.active !== true);
            done();
        });
    });

    it('Pirmo Device lauko testas pagal vartotojo name', (done) => {
        UserChar.findOne({_id: user._id}).then( (record) => {
            assert(record.devices.length !== 0);
            assert(record.devices[0].alias === 'lexus_cam');
            assert(record.devices[0].device_type === 'camera');
            assert(record.devices[0].ip_address === '172.30.453.81');
            assert(record.devices[0].active === true);
            done();
        });
    });

    it('Antro Device lauko testas pagal vartotojo name', (done) => {
        UserChar.findOne({_id: user._id}).then( (record) => {
            assert(record.devices[1].alias === 'lexus_motion');
            assert(record.devices[1].device_type === 'motion');
            assert(record.devices[1].ip_address === '172.30.453.82');
            assert(record.devices[1].active === true);
            done();
        });
    });

})