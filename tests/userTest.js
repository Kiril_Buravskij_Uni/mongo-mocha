const assert = require('assert');
const UserChar = require('../models/userListModel')

describe('Duomenu bazes User modelio testai', () => {

    var user = new UserChar({
        name: 'Antanas',
        user_type: 'premium',
        phone: +37063847554,
        active: true
    });

    it('Pridejimo testas', (done) => {
        user.save().then( (record) => {
            assert(user.isNew === false);
            assert(record.name === 'Antanas');
            assert(record.user_type !== 'premiuS');
            assert(record.phone === +37063847554);
            assert(record.active === true);
            done();
        });
    });

    it('Vardo lauko testas pagal name', (done) => {
        UserChar.findOne({name: 'Antanas'}).then( (result) => {
            assert(result.name === 'Antanas')
            done();
        });
    });
// 
    it('Vardo lauko testas pagal _id', (done) => {
        UserChar.findOne({_id: user._id}).then( (result) => {
            assert(result._id.toString() === user._id.toString())
            done();
        });
    });

    it('Vartotojo tipo lauko testas', (done) => {
        UserChar.findOne({_id: user._id}).then( (result) => {
            assert(result.user_type !== 'premiuS')
            done();
        });
    });

    it('Vartotojo telefono lauko testas', (done) => {
        UserChar.findOne({_id: user._id}).then( (result) => {
            assert(result.phone === +37063847554)
            done();
        });
    });

    it('Vartotojo statuso lauko testas', (done) => {
        UserChar.findOne({_id: user._id}).then( (result) => {
            assert(result.active === true)
            done();
        });
    });

    it('Vartotojo vardo keitimo testas', (done) => {
        UserChar.findOneAndUpdate({name: 'Antanas'}, {name: 'Robertas'}).then( () => {
            UserChar.findOne({_id: user._id}).then( (result) => {
                assert(result.name === 'Robertas')
                done();
            });
        });
    });

    it('Vartotojo istrinimas pagal _id testas', (done) => {
        UserChar.findOneAndRemove({_id: user._id}).then( () => {
            UserChar.findOne({_id: user._id}).then( (result) => {
                assert(result === null)
                done();
            });
        });
    });

})