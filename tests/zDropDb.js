const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

describe('Atsijungimo nuo duomenu bazes testas', () => {

    it('Atsijungimas', (done) => {
        mongoose.connection.close().then( () => {
            done();
            process.exit(0);
        });
    });
});
