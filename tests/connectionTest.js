const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

describe('Prisiungimo prie duomenu bazes testas', () => {

    it('Duomenu Bazes valymas', (done) => {
        mongoose.connect('mongodb://localhost/garazas', { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false });
        mongoose.connection.collections.users.drop( () => {
            done();
        });
    });

    it('Prisiungimas', (done) => {
        mongoose.connect('mongodb://localhost/garazas', { useUnifiedTopology: true, useNewUrlParser: true });
        mongoose.connection.once('open', () => {
            console.log('Sekmingai prisijungem');
            done();
          }).on('error', () => {
            console.log('Nepavyko prisijungti', error);
          });
    });
});
