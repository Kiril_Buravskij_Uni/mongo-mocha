const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const DeviceSchema = new Schema({
  alias: String,
  device_type: String,
  ip_address: String,
  active: Boolean
});

const UserSchema = new Schema({
  name: String,
  user_type: String,
  phone: Number,
  active: Boolean,
  devices: [DeviceSchema]
});

const UserChar = mongoose.model('user', UserSchema);
module.exports = UserChar;