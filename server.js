const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/garazas', { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false });

mongoose.connection.once('open', () => {
  console.log('Sekmingai prisijungem');
}).on('error', () => {
  console.log('Nepavyko prisijungti', error);
})